import NavBar from "./components/NavBar";
import Home from "./components/Home";
import "./App.css";
import "antd/dist/antd.css";
import { Layout, Col, Row, Button } from "antd";
import { useState } from "react";

// Init API
const api = {
  key: "e351fa7066620347cd632aa5efbaea2e",
  base: "https://api.openweathermap.org/data/2.5/"
}

function App() {

  const [activeComponent, setActiveComponent] = useState(<Home />);

  return (
    <Layout>
      <NavBar setActiveComponent = {setActiveComponent} />
      <Layout.Content>
        <Col offset={2} span={20} style={{marginTop:40}}>
          <Row justify="center" style={{width:'100%'}, {height:'100%'}}>{activeComponent}</Row>
        </Col>
      </Layout.Content>
    </Layout>
  );
}

export default App;

