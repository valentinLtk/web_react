import Home from "./Home";
import Login from "./Login";
import Signup from "./Signup";

import { Menu, Layout} from "antd";
import { UserOutlined, LoginOutlined, HomeOutlined } from "@ant-design/icons";

const NavBar = (props) => {

  const showItem = (item) => {
    return (
      <Menu.Item key={item.key} onClick={item.action} icon={item.icon}>
         {item.title}
      </Menu.Item>
    );
  };

  const labels = [
    {
      key: "accueil",
      action: () => props.setActiveComponent(<Home setActiveComponent={props.setActiveComponent} />),
      title: "Accueil",
      icon: <HomeOutlined />
    },
    {
      key: "connexion",
      action: () => props.setActiveComponent(<Login setActiveComponent={props.setActiveComponent} />),
      title: "Connexion",
      icon: <UserOutlined />,
    },
    {
      key: "inscription",
      action: () => props.setActiveComponent(<Signup setActiveComponent={props.setActiveComponent} />),
      title: "Inscription",
      icon: <LoginOutlined />,
    }
  ];

  return (
    <Layout.Header>
      <Menu mode="horizontal">
          {labels.map((item) => showItem(item))}
        </Menu>
    </Layout.Header>
  );
};

export default NavBar;
