import "../App.css";
import "antd/dist/antd.css";
import { useState } from "react";
import React from 'react';
import { Input } from 'antd';

// Init API
const api = {
    key: "e351fa7066620347cd632aa5efbaea2e",
    base: "https://api.openweathermap.org/data/2.5/"
}

const Home = () => {

    const [query, setQuery] = useState('');
    
    const [weather, setWeather] = useState({});


    // Envoie une requête à l'API OpenWeather
    const recherche = event => {
        fetch (`${api.base}weather?q=${query}&units=metric&lang=fr&appid=${api.key}`)
        .then(res => res.json())
        .then(result => {
            setWeather(result);
            setQuery('');
        });
    }

    return (

        // Barre de recherche et affichage dynamique du résultat
        <div>
            <main>
                <div className="search-box">

                    <Input.Search 
                        className="search-bar" 
                        placeholder="Rechercher une ville..."
                        onChange={e => setQuery(e.target.value)}
                        value={query}
                        onSearch={recherche}  
                        allowClear
                        enterButton>
                    </Input.Search>

                </div>

                {(typeof weather.main != "undefined") ? (
                <div>
                    <div className={
                            (typeof weather.main != "undefined") ? 
                            ((weather.main.temp < 15) ? 
                                "app froid" : ((weather.main.temp) > 25 ? 
                                "app chaud" : "app doux")) : "app"}>
                    <div className="location">
                        {weather.name}, {weather.sys.country}
                    </div>
                    <div className="weather-box">
                        <div className="temp"> 
                            {Math.round(weather.main.temp)}°
                        </div>
                        <div className="weather" >
                            {weather.weather[0].description}
                        </div>
                    </div>
                    </div>
                </div>
                ) : (``)}
            </main>      
        </div>
       
    )
}

export default Home