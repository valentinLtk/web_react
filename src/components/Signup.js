import Dashboard from './Dashboard';

import React, { useState } from "react";
import { Form, Input, Button, DatePicker } from "antd";

const Signup = (props) => {

  const [componentSize, setComponentSize] = useState(false);

  // Charge la page Dashboard 
  const onFinish = (values) => {
    console.log(credentials)
    props.setActiveComponent(<Dashboard/>)
  };

  return (
    <Form
      labelCol={{
        span: 10
      }}
      wrapperCol={{
        span: 6
      }}
      initialValues={{
        remember: true,
      }}
      onFinish={onFinish}
      autoComplete="off"
      style={{width:'100%'}}
    >
      <Form.Item 
        label="Prénom"
        rules={[
          {
            required: true
          }
        ]}>
        <Input />
      </Form.Item>

      <Form.Item 
        label="Nom"
        rules={[
          {
            required: true
          }
        ]}>
        <Input />
      </Form.Item>

      <Form.Item 
        label="E-mail"
        rules={[
          {
            required: true
          }
        ]}>
        <Input />
      </Form.Item>

      <Form.Item 
        label="Mot de passe"
        rules={[
          {
            required: true
          }
        ]}>
        <Input />
      </Form.Item>

      <Form.Item 
        label="Confirmer mot de passe"
        rules={[
          {
            required: true
          }
        ]}>
        <Input />
      </Form.Item>

      <Form.Item 
        label="Date de naissance"
        rules={[
          {
            required: true
          }
        ]}>
        <DatePicker />
      </Form.Item>

      <Form.Item
        wrapperCol={{
          offset: 10,
          span: 8,
        }}
      >
        <Button type="primary">
          S'inscrire
        </Button>
      </Form.Item>
    </Form>
  );
};

export default Signup;