import { identifier } from "@babel/types";
import { useQuery, gql } from "@apollo/client";

const Page = (props) => {

  const QUERY_EVENT = gql
  `
  query {
    queryEvent(first: 30) {
      id
      name
      beginsOn
    }
  }
  `
  return "Ceci est la page " + props.title;
};
  
export default Page;






  