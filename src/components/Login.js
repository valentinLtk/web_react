import Dashboard from './Dashboard';
import Signup from './Signup';

import { getAuth, signInWithEmailAndPassword } from "firebase/auth";
import { initializeApp } from '@firebase/app';
import { firebaseConfig } from "../lib/firebaseCredentials";
import { useState } from 'react';
import { Form, Input, Button, Checkbox, message } from 'antd';
import { LoadingOutlined } from '@ant-design/icons';

const firebaseApp = initializeApp(firebaseConfig);
const auth = getAuth(firebaseApp);

const Login = (props) => {

  const [loading, setLoading] = useState(false)

  const onFinish = (values) => {

    // Icone de chargement dynamique 
    setLoading(<LoadingOutlined/>)

    // Recupere le mot de passe et le mail et le 
    // transmet a la methode SignInWithEmailAndPassword
    signInWithEmailAndPassword(auth, values.email, values.password)
    .then((credentials) => {
      console.log(credentials)
      // Charge la page Dashboard 
      props.setActiveComponent(<Dashboard/>)
      setLoading(false)
    })
    .catch((err) => { 
      message.error("Email et/ou mot de passe incorrect")
      console.log(err.message)
    });
  };

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };

  return (
    <Form
      labelCol={{
        span: 10,
      }}
      wrapperCol={{
        span: 6,
      }}
      initialValues={{
        remember: true,
      }}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      autoComplete="off"
      style={{width:'100%'}}


    >
      <Form.Item
        label="E-mail"
        name="email"
        rules={[
          {
            required: true,
            message: "Veuillez entrer une adresse email",
          },
        ]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        label="Password"
        name="password"
        rules={[
          {
            required: true,
            message: "Veuillez entrer votre mot de passe",
          },
        ]}
      >
        <Input.Password />
      </Form.Item>

      <Form.Item
        name="remember"
        valuePropName="checked"
        wrapperCol={{
          offset: 10,
          span: 8,
        }}
      >
        <Checkbox>Remember me</Checkbox>
      </Form.Item>

      <Form.Item
        wrapperCol={{
          offset: 10,
          span: 8,
        }}
      >
        <Button type="primary" htmlType="submit" className="login-form-button">
          Se connecter
        </Button>
        ou <a onClick={() => props.setActiveComponent(<Signup/>)}>inscrivez-vous !</a>
      </Form.Item> 
    </Form>
  );
};

export default Login;
